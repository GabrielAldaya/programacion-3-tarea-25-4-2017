﻿using System;
using System.Net;
using System.IO;

using Newtonsoft.Json.Linq;


namespace ejercicio_1___21._4._2017
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Nombre de pelicula: ");
            string movie = Console.ReadLine();

            string search = "http://www.omdbapi.com/?t=" + movie.ToLower();
            WebRequest req = WebRequest.Create(search);

            try
                {
                WebResponse respuesta = req.GetResponse();                
           
                Stream stream = respuesta.GetResponseStream();

                StreamReader sr = new StreamReader(stream);

                JObject data = JObject.Parse(sr.ReadToEnd());

                string year =(string)data["Year"];
                if (year != null)
                {
                    string output = "La pelicula salio en el: " + (string)data["Year"];
                    Console.WriteLine(output);
                }
                else
                {
                    Console.WriteLine("La pelicula no existe");
                }                
            
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    Console.Write("No hay conexion");
                }
                else
                {
                    Console.Write("Error");
                }
            }
            Console.ReadLine();
        }

    }
}
