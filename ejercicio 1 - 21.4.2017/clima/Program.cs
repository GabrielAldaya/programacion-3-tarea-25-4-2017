﻿using System;
using System.Net;
using System.IO;

using Newtonsoft.Json.Linq;

namespace clima
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("ciudad: ");
            string ciudad = Console.ReadLine();
            string primeraparte = "https://query.yahooapis.com/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22";
            string segundaparte = "%2C%20tx%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

            string search =  primeraparte + ciudad.ToLower() + segundaparte;
            WebRequest req = WebRequest.Create(search);

            try
            {
                WebResponse respuesta = req.GetResponse();

                Stream stream = respuesta.GetResponseStream();

                StreamReader sr = new StreamReader(stream);

                JObject data = JObject.Parse(sr.ReadToEnd());


                string test = (string)data["query"]["results"]["channel"]["item"]["condition"]["text"];
                if (test != null) {
                    string output = "clima: " + test;
                    Console.WriteLine(output);
                }else
                {
                    Console.WriteLine("ingrese otra cosa");
                }
               

            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    Console.Write("No hay conexion");
                }
                else
                {
                    Console.Write("Error");
                }
            }
            Console.ReadLine();
        }

    }
}
