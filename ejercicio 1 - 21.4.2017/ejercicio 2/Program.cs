﻿using System;
using System.Net;
using System.IO;

using Newtonsoft.Json.Linq;

namespace ejercicio_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Nombre de Ciudad a buscar: ");
            string city = Console.ReadLine();

            string search = "https://maps.googleapis.com/maps/api/geocode/json?address=" + city.ToLower();
            WebRequest req = WebRequest.Create(search);

            try
            {

                WebResponse respuesta = req.GetResponse();

            Stream stream = respuesta.GetResponseStream();

            StreamReader sr = new StreamReader(stream);

            JObject data = JObject.Parse(sr.ReadToEnd());

            string country = (string)data["status"];

                if (country == "ZERO_RESULTS") {

                    Console.WriteLine("La ciudad no esta en un pais");
                }
                else if (country == "INVALID_REQUEST")
                {
                    Console.WriteLine("Estas buscando cualquiera");
                }
                else if (country == "OK")
                {
                    int x;
                    bool successfullyParsed = Int32.TryParse(city, out x);
                    if (successfullyParsed)
                    {
                        country = (string)data["results"][0]["address_components"][2]["long_name"];
                        string output = "El nombre del pais en ingles del numero que mandaste es: " + country;
                        Console.WriteLine(output);
                    }
                    else
                    {
                        country = (string)data["results"][0]["address_components"][3]["long_name"];
                        string output = "El pais de la ciudad en ingles es: " + country;
                        Console.WriteLine(output);
                    }
                }      

            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    Console.Write("No hay conexion");
                }
                else
                {
                    Console.Write("Error");
                }
            }
            Console.ReadLine();
        }

    }
}